FROM python:2-slim

LABEL   maintainer="ToM  <tom@leloop.org>" \
        org.opencontainers.image.maintainer="ToM <tom@leloop.org>" \
        org.opencontainers.image.licenses="WTFPL"

RUN pip install -U \
    Blogofile==0.7.1 \
    BeautifulSoup==3.2.2

COPY Dockerfile /
WORKDIR /source
CMD ["blogofile", "--version"]
